package com.syntel.lms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.syntel.lms.dto.Member;
import com.syntel.lms.util.DBUtil;

public class MemberDaoImpl implements MemberDao {

	@Override
	public int getMemberByType(String type) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select count(member_id) from member where type=?");
	        ps.setString(1, type);
	        rs = ps.executeQuery();
	        if(rs.next()){
	        	System.out.println(rs.getInt(1));
	        	return rs.getInt(1);
	        }
	        	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}
		return 0;
	}

	@Override
	public int getMemberCount() {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select count(member_id) from member");
	        rs = ps.executeQuery();
	        if(rs.next())
	        	return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}
		return 0;
	}

	@Override
	public boolean addMember(Member member) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("insert into member values("+member.getMemberId()+",'"+ member.getName()+"','"+member.getAddress()+"','"+member.getType()+"','"+member.getPassword()+"')");
	        ps.executeUpdate();
	        return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}
		return false;
	}

	@Override
	public boolean updateMember(Member member) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("update member set name='"+member.getName()+"', address='"+member.getAddress()+"', type='"+member.getType()+"', password='"+member.getPassword()+"' where member_id='"+member.getMemberId()+"'");
	        int flag = ps.executeUpdate();
	        System.out.println("Flag value is: "+flag);
	        if(flag>=1)
	        	return true;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

	@Override
	public boolean deleteMember(int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("delete from member where member_id="+memberId);
	        int flag = ps.executeUpdate();
	        if(flag>=1)
	        	return true;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

	@Override
	public Member getMember(int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select * from member where member_id="+memberId);
	        rs = ps.executeQuery();
	        rs.next();
	        Member member = new Member(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5));
	        System.out.println(member.toString());
	        return member;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public List<Member> getMemberList() {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<Member> memberList = new ArrayList<>();
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select * from member order by member_id");
	        rs = ps.executeQuery();
	        
	        while(rs.next()){
	        	Member m = new Member(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5));
	        	memberList.add(m);
	        }
	        return memberList;
	        
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public boolean changePassword(int memberId, String oldPassword, String newPassword) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("update member set password='"+newPassword+"' where member_id="+memberId);
	        int flag = ps.executeUpdate();
	        if(flag>=1)
	        	return true;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}		return false;
	}

	@Override
	public boolean isMemberExist(int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select * from member where member_id="+memberId);
	        rs = ps.executeQuery();
	        if(rs.next())
	        	return true;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

	@Override
	public boolean validateMember(int memberId, String password,String type) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select * from member where member_id =? and password =? and type=?");
	        ps.setInt(1,memberId);
	        ps.setString(2, password);
	        ps.setString(3, type);
	        
	        rs = ps.executeQuery();
	        if(rs.next()){
	        	System.out.println("Consider IT done..");
	        	return true;
	        }
	        	
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	
		return false;
	}

	
	
}
