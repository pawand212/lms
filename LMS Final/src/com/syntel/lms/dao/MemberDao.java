package com.syntel.lms.dao;

import java.util.List;

import com.syntel.lms.dto.Member;

public interface MemberDao {

	boolean addMember(Member member);
	boolean updateMember(Member member);
	boolean deleteMember(int memberId);
	Member getMember(int memberId);
	List<Member> getMemberList();
	boolean changePassword(int memberId,String oldPassword, String newPassword);
	boolean isMemberExist(int memberId);
	boolean validateMember(int memberId,String password,String type);	
	int getMemberByType(String type);
	int getMemberCount();
	
}
