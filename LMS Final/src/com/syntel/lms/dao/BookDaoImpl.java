package com.syntel.lms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.syntel.lms.dto.Book;
import com.syntel.lms.dto.BorrowedBook;
import com.syntel.lms.dto.Member;
import com.syntel.lms.util.DBUtil;

public class BookDaoImpl implements BookDao{

	@Override
	public int getBookByStatus(String status) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select count(accession_no) from book where status=?");
	        ps.setString(1, status);
	        rs = ps.executeQuery();
	        if(rs.next())
	        	return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}
		return 0;
	}

	@Override
	public int getBookCount() {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select count(accession_no) from book");
	        rs = ps.executeQuery();
	        if(rs.next())
	        	return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}
		return 0;
	}

	@Override
	public boolean addBook(Book book) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("insert into book values(?,?,?,?)");
	        ps.setString(1,book.getAccessionNo());
	        ps.setString(2, book.getTitle());
	        ps.setString(3,book.getAuthor());
	        ps.setString(4, book.getStatus());
	        
	        int flag = ps.executeUpdate();
	        System.out.println(flag);
	        if(flag>=1){
	        	return true;
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}
	
		return false;
	}

	@Override
	public boolean updateBookStatus(String status,String accessionNo) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
				ps=con.prepareStatement("update book set status=? where accession_no =?");
				ps.setString(1,status);
				ps.setString(2, accessionNo);
				
				int flag = ps.executeUpdate();
		        if(flag>=1){
		        	System.out.println("Consider IT done..");
		        	return true;
		        }
	        
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
			
			
			
			
	}

	@Override
	public boolean deleteBook(String accessionNo) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("delete from book where accession_no =?");
	        ps.setString(1,accessionNo);
	    	int flag = ps.executeUpdate();
	        if(flag>=1){
	        	return true;
	        }
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

	@Override
	public List<Book> getBookList() {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<Book> bookList = new ArrayList<>();
		try {
			con=DBUtil.getConnection();
			ps=con.prepareStatement("select * from book order by accession_no");
			 rs = ps.executeQuery();
		        
		        while(rs.next()){
		        	Book b = new Book(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
		        	bookList.add(b);
		        }
		  
		 
		        return bookList;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public boolean issueBook(String accessionNo, int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("");
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

	@Override
	public boolean returnBook(String accessionNo, int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("");
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

	@Override
	public Book getBook(String accessionNo) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		Book book = null;
		try {
			System.out.println("Here we goo...");
			con=DBUtil.getConnection();
			ps=con.prepareStatement("select * from book where accession_no=?");
			ps.setString(1,	accessionNo);
	        rs = ps.executeQuery();
	        while(rs.next())
	        	book = new Book(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
	        System.out.println(book.toString());
	        return book;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public List<Book> getBookListByAuthor(String author) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<Book> bookList = new ArrayList<>();
		try {
			con=DBUtil.getConnection();
			 ps=con.prepareStatement("select * from book where author='"+author+"' order by accession_no");
			 //ps.setString(1, author);
			 System.out.println("Query executed....");
			   rs = ps.executeQuery();
		        
		        while(rs.next()){
		        	System.out.println("In rs.next()"+rs);
		        	Book b = new Book(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
		        	bookList.add(b);
		        }
		  
		 
		        return bookList;
		       }
		
		        catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public List<Book> getBookListByTitle(String title) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<Book> bookList = new ArrayList<>();
		try {
			con=DBUtil.getConnection();
			ps=con.prepareStatement("select * from book where title=? order by accession_no");
			 ps.setString(1, title);
			   rs = ps.executeQuery();
		        
		        while(rs.next()){
		        	Book b = new Book(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
		        	bookList.add(b);
		        }
		  
		 
		        return bookList;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public List<BorrowedBook> getBorrowedBookListByMember(int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
			List<BorrowedBook> list = new ArrayList<>();
	        ps=con.prepareStatement("select b.title,t.issue_date,t.due_date,t.return_date,t.fine_amt,t.status "
	        		+ "from book b join transaction t on t.accession_no=b.accession_no and t.member_id=? order by b.title");
	        ps.setInt(1, memberId);
	        rs = ps.executeQuery();
	        while(rs.next()) {
	        	BorrowedBook bb = new BorrowedBook(rs.getString(1), rs.getDate(2), rs.getDate(3), rs.getDate(4), rs.getLong(5), rs.getString(6));
	        	list.add(bb);
	        }
	        return list;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return null;
	}

	@Override
	public int getNoOfBorrowedBooksByMember(int memberId) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select count(transaction_id) from transaction where member_id=? and status='Open'");
	        ps.setInt(1, memberId);
	        
	        rs = ps.executeQuery();
	        int count=0;
	        if(rs.next())
	        	count = rs.getInt(1);
	        System.out.println(count);
	        return count;
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}		return 0;
	}

	@Override
	public boolean isBookAvailable(String accessionNo) {
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			con=DBUtil.getConnection();
	        ps=con.prepareStatement("select status from book where accession_no=?");
	        ps.setString(1, accessionNo);
	        rs = ps.executeQuery();
	        System.out.println("Querry executed");
	        if(rs.next()) {
	        	System.out.println("In  rs.next()");
	        	System.out.println(rs.getString(1));
	        	if(rs.getString(1).equals("Available")) {
	        		System.out.println("In available");
	        		return true;
	        	}
	        		
	        	else
	        		return false;
	        }
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			DBUtil.close(con);
			}	return false;
	}

}
