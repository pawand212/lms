package com.syntel.lms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.sql.Date;

import com.syntel.lms.dto.Transaction;
import com.syntel.lms.util.DBUtil;

public class TransactionDaoImpl implements TransactionDao {

	@Override
	public boolean getTransactionByAccessionNo(String accessionNo) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = DBUtil.getConnection();
			ps = con.prepareStatement("select * from transaction where accession_no=? and status='Open'");
			ps.setString(1, accessionNo);
			rs = ps.executeQuery();
			while(rs.next())
				return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return false;
	}

	@Override
	public boolean getTransactionByMemberId(int memberId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = DBUtil.getConnection();
			ps = con.prepareStatement("select * from transaction where member_id=? and status='Open' order by transaction_id");
			ps.setInt(1, memberId);
			rs = ps.executeQuery();
			while(rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return false;
	}

	@Override
	public Transaction getTransactionById(String transactionId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DBUtil.getConnection();
			ps = con.prepareStatement("select * from transaction where transaction_id=? order by transaction_id");
			ps.setString(1, transactionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				return new Transaction(rs.getString(1), rs.getInt(2),
						rs.getString(3), rs.getDate(4), rs.getDate(5),
						rs.getDate(6), rs.getInt(7), rs.getString(8));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return null;
	}

	@Override
	public int calculateLateFees(int memberId, String accessionNo) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = DBUtil.getConnection();
			ps = con.prepareStatement("");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return 0;
	}

	@Override
	public String createTransaction(int memberId, String accessionNo) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DBUtil.getConnection();
			System.out.println("In DAO Class");
			ps = con.prepareStatement("insert into transaction(transaction_id,member_id,accession_no,issue_date,due_date,status) values('TSC_'||tsc.nextval,?,?,sysdate,sysdate+7,?)");
			ps.setInt(1, memberId);
			ps.setString(2, accessionNo);
			ps.setString(3, "Open");
			if (ps.executeUpdate() == 1) {
				return "Book issued successfully.....";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return "Book issue Failed.......";
	}

	@Override
	public boolean closeTransaction(String transactionId,Date returnDate,long fineAmount) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = DBUtil.getConnection();
			ps = con.prepareStatement("update transaction set status='Close',return_date=?,fine_amt=? where transaction_id=?");
			ps.setDate(1, returnDate);
			ps.setLong(2, fineAmount);
			ps.setString(3, transactionId);
			if(ps.executeUpdate()>=1)
				return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return false;
	}

	@Override
	public List<Transaction> getAllTransactionList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			System.out.println("In getAllTransaction() method.....");
			List<Transaction> transactionList = new ArrayList<>();
			con = DBUtil.getConnection();
			ps = con.prepareStatement("select * from transaction order by transaction_id");
			rs = ps.executeQuery();
			while (rs.next()) {
				Transaction transaction = new Transaction(rs.getString(1),
						rs.getInt(2), rs.getString(3), rs.getDate(4),
						rs.getDate(5), rs.getDate(6), rs.getInt(7),
						rs.getString(8));
				transactionList.add(transaction);
			}
			return transactionList;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtil.close(con);
		}
		return null;
	}

}
