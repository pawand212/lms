package com.syntel.lms.dao;

import java.util.List;

import com.syntel.lms.dto.Transaction;

public interface TransactionDao {

	int calculateLateFees(int memberId,String accessionNo);
	String createTransaction(int memberId,String accessionNo);
	boolean closeTransaction(String transactionId,java.sql.Date returnDate,long fineAmount);
    List<Transaction> getAllTransactionList();
   Transaction getTransactionById(String transactionId);
   boolean getTransactionByMemberId(int memberId);
   boolean getTransactionByAccessionNo(String accessionNo);
}
