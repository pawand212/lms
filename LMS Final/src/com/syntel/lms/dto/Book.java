package com.syntel.lms.dto;

import java.io.Serializable;

public class Book implements Serializable {

	private String accessionNo;
	private String title;
	private String author;
	private String status;

	public Book() {
	}

	public Book(String accessionNo, String title, String author, String status) {
		super();
		this.accessionNo = accessionNo;
		this.title = title;
		this.author = author;
		this.status = status;
	}

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Book [accessionNo=" + accessionNo + ", title=" + title + ", author=" + author + ", status=" + status
				+ "]";
	}

}
