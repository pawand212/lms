package com.syntel.lms.dto;

import java.sql.Date;

public class BorrowedBook {
 private String title;
 private Date issueDate;
 private Date dueDate;
 private Date returnDate;
 private long fineAmount;
 private String status;
 
 
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public Date getIssueDate() {
	return issueDate;
}
public void setIssueDate(Date issueDate) {
	this.issueDate = issueDate;
}
public Date getDueDate() {
	return dueDate;
}
public void setDueDate(Date dueDate) {
	this.dueDate = dueDate;
}
public Date getReturnDate() {
	return returnDate;
}
public void setReturnDate(Date returnDate) {
	this.returnDate = returnDate;
}
public long getFineAmount() {
	return fineAmount;
}
public void setFineAmount(long fineAmount) {
	this.fineAmount = fineAmount;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public BorrowedBook(String title, Date issueDate, Date dueDate, Date returnDate, long fineAmount, String status) {
	super();
	this.title = title;
	this.issueDate = issueDate;
	this.dueDate = dueDate;
	this.returnDate = returnDate;
	this.fineAmount = fineAmount;
	this.status = status;
}
 
 
 
}
