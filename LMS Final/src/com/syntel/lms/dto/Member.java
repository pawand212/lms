package com.syntel.lms.dto;

import java.io.Serializable;

public class Member implements Serializable {

	private int memberId;
	private String name;
	private String address;
	private String type;
	private String password;

	public Member() {

	}

	public Member(int memberId, String name, String address, String type, String password) {
		super();
		this.memberId = memberId;
		this.name = name;
		this.address = address;
		this.type = type;
		this.password = password;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Member [memberId=" + memberId + ", name=" + name + ", address=" + address + ", type=" + type
				+ ", password=" + password + "]";
	}

}
