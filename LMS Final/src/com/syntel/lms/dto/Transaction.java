package com.syntel.lms.dto;

import java.io.Serializable;
import java.sql.Date;

public class Transaction implements Serializable{

private String transactionId;
private int memberId;
private String accessionNo;
private Date issueDate;
private Date dueDate;
private Date returnDate;
private long fineAmount;
private String status;

public Transaction() {
}

public Transaction(String transactionId, int memberId, String accessionNo, Date issueDate, Date dueDate,
		Date returnDate, long fineAmount, String status) {
	super();
	this.transactionId = transactionId;
	this.memberId = memberId;
	this.accessionNo = accessionNo;
	this.issueDate = issueDate;
	this.dueDate = dueDate;
	this.returnDate = returnDate;
	this.fineAmount = fineAmount;
	this.status = status;
}

public String getTransactionId() {
	return transactionId;
}

public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
}

public int getMemberId() {
	return memberId;
}

public void setMemberId(int memberId) {
	this.memberId = memberId;
}

public String getAccessionNo() {
	return accessionNo;
}

public void setAccessionNo(String accessionNo) {
	this.accessionNo = accessionNo;
}

public Date getIssueDate() {
	return issueDate;
}

public void setIssueDate(Date issueDate) {
	this.issueDate = issueDate;
}

public Date getDueDate() {
	return dueDate;
}

public void setDueDate(Date dueDate) {
	this.dueDate = dueDate;
}

public Date getReturnDate() {
	return returnDate;
}

public void setReturnDate(Date returnDate) {
	this.returnDate = returnDate;
}

public long getFineAmount() {
	return fineAmount;
}

public void setFineAmount(long fineAmount) {
	this.fineAmount = fineAmount;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

@Override
public String toString() {
	return "Transaction [transactionId=" + transactionId + ", memberId=" + memberId + ", accessionNo=" + accessionNo
			+ ", issueDate=" + issueDate + ", dueDate=" + dueDate + ", returnDate=" + returnDate + ", fineAmount="
			+ fineAmount + ", status=" + status + "]";
}

}
