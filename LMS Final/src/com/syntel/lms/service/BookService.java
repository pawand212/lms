package com.syntel.lms.service;

import java.util.List;

import com.syntel.lms.dto.Book;
import com.syntel.lms.dto.BorrowedBook;

public interface BookService {
	boolean addBook(Book book);
	boolean updateBookStatus(String status, String accessionNo);
	boolean deleteBook(String accessionNo);
	List<Book> getBookList();
	boolean issueBook(String accessionNo,int memberId);
	boolean returnBook(String accessionNo,int memberId);
	Book getBook(String accessionNo);
	List<Book> getBookListByAuthor(String author);
	List<Book> getBookListByTitle(String title);
	List<BorrowedBook> getBorrowedBookListByMember(int memberId);
	int getNoOfBorrowedBooksByMember(int memberId);
	boolean isBookAvailable(String accessionNo);
	int getBookByStatus(String status);
	int getBookCount();
	
}
