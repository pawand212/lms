package com.syntel.lms.service;

import java.util.List;

import com.syntel.lms.dao.MemberDao;
import com.syntel.lms.dao.MemberDaoImpl;
import com.syntel.lms.dto.Member;

public class MemberServiceImpl implements MemberService {

	@Override
	public int getMemberByType(String type) {
		return memberDao.getMemberByType(type);
	}




	@Override
	public int getMemberCount() {
		return memberDao.getMemberCount();
	}




	//dependency
	private MemberDao memberDao = new MemberDaoImpl();
	
	
	//dependency injection
	public void setMemberDao(MemberDao memberDao) {
		this.memberDao = memberDao;
	}
	
	
	
	
	@Override
	public boolean addMember(Member member) {
		return memberDao.addMember(member);
	}

	@Override
	public boolean updateMember(Member member) {
		return memberDao.updateMember(member);
	}

	@Override
	public boolean deleteMember(int memberId) {
		return memberDao.deleteMember(memberId);
	}

	@Override
	public Member getMember(int memberId) {
		return memberDao.getMember(memberId);
	}

	@Override
	public List<Member> getMemberList() {
		// TODO Auto-generated method stub
		return memberDao.getMemberList();
	}

	@Override
	public boolean changePassword(int memberId, String oldPassword, String newPassword) {
		return memberDao.changePassword(memberId, oldPassword, newPassword);
	}

	@Override
	public boolean isMemberExist(int memberId) {
		return memberDao.isMemberExist(memberId);
	}




	@Override
	public boolean validateMember(int memberId, String password, String type) {
		
		return memberDao.validateMember(memberId, password, type);
	}

	
	
}
