package com.syntel.lms.service;

import java.util.List;

import com.syntel.lms.dao.BookDao;
import com.syntel.lms.dao.BookDaoImpl;
import com.syntel.lms.dto.Book;
import com.syntel.lms.dto.BorrowedBook;

public class BookServiceImpl implements BookService{

	@Override
	public int getBookByStatus(String status) {
		return bookDao.getBookByStatus(status);
	}




	@Override
	public int getBookCount() {
		return bookDao.getBookCount();
	}

	//dependency
	private BookDao bookDao=new BookDaoImpl();
	
	
	//dependency injection
	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	
	
	
	
	@Override
	public boolean addBook(Book book) {
		return bookDao.addBook(book);
	}

	@Override
	public boolean updateBookStatus(String status, String accessionNo) {
		return bookDao.updateBookStatus(status, accessionNo);
	}

	@Override
	public boolean deleteBook(String accessionNo) {
		return bookDao.deleteBook(accessionNo);
	}

	@Override
	public List<Book> getBookList() {
		return bookDao.getBookList();
	}

	@Override
	public boolean issueBook(String accessionNo, int memberId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean returnBook(String accessionNo, int memberId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Book getBook(String accessionNo) {
		return bookDao.getBook(accessionNo);
	}

	@Override
	public List<Book> getBookListByAuthor(String author) {
		return bookDao.getBookListByAuthor(author);
	}

	@Override
	public List<Book> getBookListByTitle(String title) {
		return bookDao.getBookListByTitle(title);
	}

	@Override
	public List<BorrowedBook> getBorrowedBookListByMember(int memberId) {
		return bookDao.getBorrowedBookListByMember(memberId);
	}

	@Override
	public int getNoOfBorrowedBooksByMember(int memberId) {
		return bookDao.getNoOfBorrowedBooksByMember(memberId);
	}

	@Override
	public boolean isBookAvailable(String accessionNo) {
		return bookDao.isBookAvailable(accessionNo);
	}

	
	
}
