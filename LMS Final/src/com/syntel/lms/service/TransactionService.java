package com.syntel.lms.service;

import java.sql.Date;
import java.util.List;

import com.syntel.lms.dto.Transaction;

public interface TransactionService {
	int calculateLateFees(int memberId,String accessionNo);
	String createTransaction(int memberId,String accessionNo);
	boolean closeTransaction(String transactionId,Date returnDate,long fineAmount);
    List<Transaction> getAllTransactionList();
    Transaction getTransactionById(String transactionId);
    boolean getTransactionByMemberId(int memberId);
    boolean getTransactionByAccessionNo(String accessionNo);
}
