package com.syntel.lms.service;

import java.sql.Date;
import java.util.List;

import com.syntel.lms.dao.TransactionDao;
import com.syntel.lms.dao.TransactionDaoImpl;
import com.syntel.lms.dto.Transaction;

public class TransactionServiceImpl implements TransactionService{

	@Override
	public boolean getTransactionByAccessionNo(String accessionNo) {
		return transactionDao.getTransactionByAccessionNo(accessionNo);
	}


	@Override
	public boolean getTransactionByMemberId(int memberId) {
		return transactionDao.getTransactionByMemberId(memberId);
	}


	@Override
	public Transaction getTransactionById(String transactionId) {
		return transactionDao.getTransactionById(transactionId);
	}

	//dependency
	private TransactionDao transactionDao = new TransactionDaoImpl();
	
	
	//dependency injection
	public void setTransactionDao(TransactionDao transactionDao) {
		this.transactionDao = transactionDao;
	}
	
	
	@Override
	public int calculateLateFees(int memberId, String accessionNo) {
		return transactionDao.calculateLateFees(memberId, accessionNo);
	}

	@Override
	public String createTransaction(int memberId, String accessionNo) {
		return transactionDao.createTransaction(memberId, accessionNo);
	}

	@Override
	public boolean closeTransaction(String transactionId,Date returnDate,long fineAmount) {
		return transactionDao.closeTransaction(transactionId,returnDate,fineAmount);
	}

	@Override
	public List<Transaction> getAllTransactionList() {
		return transactionDao.getAllTransactionList();
	}

	
	
}
