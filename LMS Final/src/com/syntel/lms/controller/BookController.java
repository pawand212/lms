package com.syntel.lms.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.syntel.lms.dto.Book;
import com.syntel.lms.service.BookService;
import com.syntel.lms.service.BookServiceImpl;
import com.syntel.lms.service.TransactionService;
import com.syntel.lms.service.TransactionServiceImpl;

public class BookController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	BookService bookService =null;
	TransactionService transactionService = null;
    public BookController() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public void init() throws ServletException {
		bookService = new BookServiceImpl();
		transactionService = new TransactionServiceImpl();
		System.out.println("BookService initialized..");
	}
	
	@Override
	public void destroy() {
		bookService=null;
		transactionService=null;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = null;
		String action = request.getParameter("action");
		
		if(action.equals("GetBookList")){
			System.out.println("In get book list...");
			request.setAttribute("value", "viewallbooks");
			request.setAttribute("bookList", bookService.getBookList());
			if(request.getSession().getAttribute("memberType").equals("Librarian")) {
				request.setAttribute("message", "List of Books");
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			}
			else {
				request.setAttribute("message", "List of Books");
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			}
				
			
		}
		else if(action.equals("ADDBOOK")){
			request.setAttribute("value", "addbook");
			request.setAttribute("message", "You are not Librarian!!!");
			if(request.getSession().getAttribute("memberType").equals("Librarian")) {
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			}
			else
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			
		}
		else if(action.equals("UPDATEBOOK")){
			request.setAttribute("value", "updatebook");
			request.setAttribute("message", "You are not Librarian!!!");
			if(request.getSession().getAttribute("memberType").equals("Librarian")) {
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			}
			else
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			
		}
		else if(action.equals("DELETEBOOK")){
			request.setAttribute("value", "deletebook");
			request.setAttribute("bookList", bookService.getBookList());
			
			if(request.getSession().getAttribute("memberType").equals("Librarian")) {
				request.setAttribute("message", "Select books to delete");
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			}
			else{
				request.setAttribute("message", "You are not Librarian!!!");
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			}
				
			
		}
		else if(action.equals("GETBOOKBYAUTHOR")){
			request.setAttribute("value", "getbookbyauthor");
			request.setAttribute("bookList", bookService.getBookList());
			HttpSession hs = request.getSession();
			
			if(hs.getAttribute("memberType").equals("Librarian"))
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			else
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			
		}
		else if(action.equals("GETBOOKBYTITLE")){
			request.setAttribute("value", "getbookbytitle");
			request.setAttribute("bookList", bookService.getBookList());
			if(request.getSession().getAttribute("memberType").equals("Librarian")) {
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			}
			else
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			
		}
		
		else if(action.equals("ViewBorrowedBooks")){
			HttpSession hs = request.getSession();
			int memberId = Integer.parseInt(hs.getAttribute("memberId").toString());
			
			request.setAttribute("bookList", bookService.getBorrowedBookListByMember(memberId));
			request.setAttribute("value", "borrowedbooklist");
			request.setAttribute("message", "List of books borrowd by You!");
			request.getRequestDispatcher("memberhome.jsp").forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd = null;
		String action = request.getParameter("action");
		
		if(action.equals("AddNewBook")){
			System.out.println("In add book");
			String accessionNo = request.getParameter("accessionNo");
			String title = request.getParameter("title");
			String author = request.getParameter("author");
			String status = "Available";
			Book book = new Book(accessionNo, title, author, status);
			if(bookService.addBook(book)){
				request.setAttribute("message", "Book added successfully");
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			}
			else
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);		
		}
		
		else if(action.equals("DeleteBook")){
			System.out.println("In Delete book");
			String[] accessionNoList = request.getParameterValues("element");
			String finalMsg="";
			for(int i=0;i<accessionNoList.length;i++) {
				if(transactionService.getTransactionByAccessionNo(accessionNoList[i])) {
					finalMsg+=(" Book : "+bookService.getBook(accessionNoList[i]).getTitle()+" is under transaction\n");
				}
				else {
					String bname = bookService.getBook(accessionNoList[i]).getTitle();
					if(bookService.deleteBook(accessionNoList[i])) {
						finalMsg+=(" Book : "+bname+" deleted successfully\n");
					}
					else
						finalMsg+=(" Book : "+bname+" is in transaction table with close status\n");
					
				}
			}
			
				request.setAttribute("message", finalMsg);
				rd = request.getRequestDispatcher("librarianhome.jsp");
				rd.forward(request, response);
			
				
			
		}
		
		else if(action.equals("DisplayBookByAuthor")){
			System.out.println("In Display book by author.......");
			
			String author = request.getParameter("authorName");
			System.out.println(author);
			request.setAttribute("bookList", bookService.getBookListByAuthor(author));
			request.setAttribute("value", "dbba");
			request.setAttribute("message", "Book List By Author");
			if(request.getSession().getAttribute("memberType").equals("Librarian"))
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			else
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
			
		}
		else if(action.equals("DisplayBookByTitle")){
			System.out.println("In Display book by title.......");
			
			String title = request.getParameter("title");
			System.out.println(title);
			request.setAttribute("bookList", bookService.getBookListByTitle(title));
			request.setAttribute("value", "dbbt");
			request.setAttribute("message", "Book List By Title");
			if(request.getSession().getAttribute("memberType").equals("Librarian"))
				request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
			else
				request.getRequestDispatcher("memberhome.jsp").forward(request, response);
		}
		
		else if(action.equals("UpdateBookStatus")){
			String accno = request.getParameter("accno");
			String status = request.getParameter("status");
			
			if(bookService.updateBookStatus(status, accno)){
				 request.setAttribute("message", "Book status updated successfully...");
			}
			else
				request.setAttribute("message", "Book status updated failed...");
			request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
		}
	}

}
