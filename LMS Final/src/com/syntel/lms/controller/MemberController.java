package com.syntel.lms.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.syntel.lms.dto.Member;
import com.syntel.lms.service.BookService;
import com.syntel.lms.service.BookServiceImpl;
import com.syntel.lms.service.MemberService;
import com.syntel.lms.service.MemberServiceImpl;
import com.syntel.lms.service.TransactionService;
import com.syntel.lms.service.TransactionServiceImpl;

public class MemberController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MemberService memberService;
	private BookService bookService;
	private TransactionService transactionService;

	public MemberController() {
		System.out.println("MemberController Created");
	}

	@Override
	public void init() throws ServletException {
		memberService = new MemberServiceImpl();
		bookService = new BookServiceImpl();
		transactionService = new TransactionServiceImpl();
		System.out.println("MemberService initialized..");
	}

	@Override
	public void destroy() {
		memberService = null;
		bookService=null;
		transactionService = null;
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		RequestDispatcher rd = null;
		System.out.println(action);
		if (action.equals("ViewAllMembers")) {
			System.out.println("In View all members.........................");
			request.setAttribute("memberList", memberService.getMemberList());
			request.setAttribute("value", "viewallmembers");
			if (request.getSession().getAttribute("memberType")
					.equals("Librarian")) {
				request.setAttribute("message", "Members List");
				rd = request.getRequestDispatcher("librarianhome.jsp");
			} else {
				request.setAttribute("message", "You are not Librarian!!!");
				rd = request.getRequestDispatcher("memberhome.jsp");
			}
			rd.forward(request, response);
		}

		else if (action.equals("ADDMEMBER")) {
			HttpSession hs = request.getSession();
			if (hs.getAttribute("memberType").equals("Librarian")) {
				request.setAttribute("value", "addmnewember");
				rd = request.getRequestDispatcher("librarianhome.jsp");
				rd.forward(request, response);
			} else {

				request.setAttribute("message", "You are not Librarian...");
				rd = request.getRequestDispatcher("memberhome.jsp");
				rd.forward(request, response);
			}

		}
		
		
		else if(action.equals("CHANGEPASSWORD")){
			HttpSession hs = request.getSession();
			if (hs.getAttribute("memberType").equals("Librarian")) {
				request.setAttribute("value", "changepassword");
				rd = request.getRequestDispatcher("librarianhome.jsp");
				rd.forward(request, response);
			} else {

				request.setAttribute("value", "changepassword");
				rd = request.getRequestDispatcher("memberhome.jsp");
				rd.forward(request, response);
			}
		}
		
		else if(action.equals("HOME")){
			HttpSession hs = request.getSession();
			System.out.println("In home");
			if (hs.getAttribute("memberType").equals("Librarian")) {
				request.setAttribute("flag", "show");
				request.setAttribute("noOfStudent", memberService.getMemberByType("Student"));
				request.setAttribute("noOfStaff", memberService.getMemberByType("Staff"));
				request.setAttribute("totalMembers", memberService.getMemberCount());
				request.setAttribute("booksAvailable", bookService.getBookByStatus("Available"));
				request.setAttribute("booksNotAvailable", bookService.getBookByStatus("Not Available"));
				request.setAttribute("totalBooks", bookService.getBookCount());
				rd = request.getRequestDispatcher("librarianhome.jsp");
				rd.forward(request, response);
			} else {

				request.setAttribute("flag", "show");
				request.setAttribute("booksAvailable", bookService.getBookByStatus("Available"));
				request.setAttribute("booksNotAvailable", bookService.getBookByStatus("Not Available"));
				request.setAttribute("totalBooks", bookService.getBookCount());
				rd = request.getRequestDispatcher("memberhome.jsp");
				rd.forward(request, response);
			}
		}

		else if (action.equals("UPDATEMEMBER")) {
			request.setAttribute("value", "updatemember");
			request.setAttribute("memberList", memberService.getMemberList());
			if (request.getSession().getAttribute("memberType")
					.equals("Librarian")) {
				request.getRequestDispatcher("librarianhome.jsp").forward(
						request, response);
			} else {
				request.setAttribute("message", "You are not Librarian!!!");
				request.getRequestDispatcher("memberhome.jsp").forward(request,
						response);
			}
		}

		else if (action.equals("DELETEMEMBER")) {
			request.setAttribute("value", "deletemember");
			request.setAttribute("memberList", memberService.getMemberList());
			if (request.getSession().getAttribute("memberType")
					.equals("Librarian")) {
				request.setAttribute("message", "Select members you want to delete");
				request.getRequestDispatcher("librarianhome.jsp").forward(
						request, response);
			} else {
				request.setAttribute("message", "You are not Librarian!!!");
				request.getRequestDispatcher("memberhome.jsp").forward(request,
						response);
			}
		}

		else if (action.equals("LOGOUT")) {
			request.setAttribute("value", "logout");
			request.getRequestDispatcher("logout.jsp").forward(request,
					response);
		}

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = null;
		String action = request.getParameter("action");

		if (action.equals("Login")) {
			System.out.println("In Login................................");
			int memberId = Integer.parseInt(request.getParameter("memberId"));
			String password = request.getParameter("password");
			String type = request.getParameter("type");

			boolean isValid = memberService.validateMember(memberId, password,
					type);

			if (isValid) {
				HttpSession hs = request.getSession();
				hs.setAttribute("memberType", type);
				hs.setAttribute("memberId", memberId);
				hs.setAttribute("librarianName", memberService.getMember(memberId).getName());
				hs.setAttribute("memberName", memberService.getMember(memberId).getName());

				if (type.equalsIgnoreCase("Librarian")) {
					request.setAttribute("librarianName", memberService
							.getMember(memberId).getName());
					request.setAttribute("flag", "show");
					request.setAttribute("noOfStudent", memberService.getMemberByType("Student"));
					request.setAttribute("noOfStaff", memberService.getMemberByType("Staff"));
					request.setAttribute("totalMembers", memberService.getMemberCount());
					request.setAttribute("booksAvailable", bookService.getBookByStatus("Available"));
					request.setAttribute("booksNotAvailable", bookService.getBookByStatus("Not Available"));
					request.setAttribute("totalBooks", bookService.getBookCount());
					rd = request.getRequestDispatcher("librarianhome.jsp");
					
				}

				else if (type.equals("Staff") || type.equals("Student")) {
					request.setAttribute("memberName",
							memberService.getMember(memberId).getName());
					request.setAttribute("booksAvailable", bookService.getBookByStatus("Available"));
					request.setAttribute("booksNotAvailable", bookService.getBookByStatus("Not Available"));
					request.setAttribute("totalBooks", bookService.getBookCount());
					request.setAttribute("flag", "show");
					rd = request.getRequestDispatcher("memberhome.jsp");
				}

			} else {

				request.setAttribute("message", "Invalid Login Details...");
				rd = request.getRequestDispatcher("index.jsp");
			}
			rd.forward(request, response);
		} else if (action.equals("AddNewMember")) {

			HttpSession hs = request.getSession();
			if (hs.getAttribute("memberType").equals("Librarian")) {
				System.out.println("In add new member........................");
				int memberId = Integer.parseInt(request
						.getParameter("memberId"));
				String password = request.getParameter("password");
				String type = request.getParameter("type");
				String memberName = request.getParameter("memberName");
				String memberAddress = request.getParameter("memberAddress");

				Member member = new Member(memberId, memberName, memberAddress,
						type, password);
				if (memberService.addMember(member)) {
					request.setAttribute("message",
							"Member added successfully...");
					rd = request.getRequestDispatcher("librarianhome.jsp");
				} else {
					rd = request.getRequestDispatcher("librarianhome.jsp");
				}
				rd.forward(request, response);
			} else {
				rd = request.getRequestDispatcher("memberhome.jsp");
				request.setAttribute("message", "You are not Librarian...");
				rd.forward(request, response);
			}
		}

		else if (action.equals("DeleteMember")) {
			System.out.println("In delete member........................");
			String[] list = request.getParameterValues("element");
			String msg = "";
			for (int i = 0; i < list.length; i++) {
				if (!transactionService.getTransactionByMemberId(Integer
						.parseInt(list[i]))) {
					String name = memberService.getMember(
							Integer.parseInt(list[i])).getName();
					if (memberService.deleteMember(Integer.parseInt(list[i]))) {
						msg += " Member : " + name
								+ " deleted successfully...\n";
					} else {
						msg += " Member: "
								+ name
								+ " can't be removed first remove his/her transaction records\n";
					}
				} else {
					msg += " Member: "
							+ memberService
									.getMember(Integer.parseInt(list[i]))
									.getName() + " is Under Transaction\n";
				}

			}
			request.setAttribute("message", msg);
			request.getRequestDispatcher("librarianhome.jsp").forward(request,
					response);
		}

		else if (action.equals("GetMemberDetails")) {
			System.out.println("In Get Member Details............");
			int memberId = Integer.parseInt(request.getParameter("memberId"));
			System.out.println(memberId);
			request.setAttribute("memberDetails",
					memberService.getMember(memberId));
			request.setAttribute("message", "Member Details");
			request.getRequestDispatcher("librarianhome.jsp").forward(request,
					response);
		}

		else if (action.equals("UpdateMember")) {
			System.out.println("In update member details............");
			int memberId = Integer.parseInt(request.getParameter("element"));
			String password = request.getParameter("password");
			String type = request.getParameter("type");
			String memberName = request.getParameter("memberName");
			String memberAddress = request.getParameter("memberAddress");

			Member member = new Member(memberId, memberName, memberAddress,
					type, password);
			if (memberService.updateMember(member)) {
				request.setAttribute("message", "Member Updated successfully");
				rd = request.getRequestDispatcher("librarianhome.jsp");
			} else {
				rd = request.getRequestDispatcher("librarianhome.jsp");
			}
			rd.forward(request, response);
		} 
		
		
		else if (action.equals("ChangePassword")) {
			System.out.println("In change password.........");
			HttpSession hs = request.getSession();
			int memberId = Integer.parseInt(hs.getAttribute("memberId").toString());
			String oldPassword = request.getParameter("cPassword");
			String newPassword1 = request.getParameter("nPassword1");
			String newPassword2 = request.getParameter("nPassword2");

			if (memberService.isMemberExist(memberId)) {
				System.out.println("Member exists......");
				Member member = memberService.getMember(memberId);
				System.out.println(member.getPassword());
				System.out.println(oldPassword);
				System.out.println(member.getPassword().equals(oldPassword));
				if (member.getPassword().equals(oldPassword)) {
					
					if(newPassword1.equals(newPassword2)){
						memberService.changePassword(memberId, oldPassword, newPassword1);
						request.setAttribute("message", "Password Changed Successfully...");
						if(memberService.getMember(memberId).getType().equals("Librarian")){
							rd = request.getRequestDispatcher("librarianhome.jsp");
						}
						else
							rd=request.getRequestDispatcher("memberhome.jsp");
					}
					else{
						request.setAttribute("message", "New passwords are not mathching");
						if(memberService.getMember(memberId).getType().equals("Librarian")){
							rd=request.getRequestDispatcher("librarianhome.jsp");
						}
						else
							rd = request.getRequestDispatcher("memberhome.jsp");
					}
					
				} 
				else {
					request.setAttribute("message", "Your entered password and old password are not matching");
					if(memberService.getMember(memberId).getType().equals("Librarian")){
						rd=request.getRequestDispatcher("librarianhome.jsp");
					}
					else
						rd=request.getRequestDispatcher("memberhome.jsp");
				}
			}
			rd.forward(request, response);
		}
	}

}
