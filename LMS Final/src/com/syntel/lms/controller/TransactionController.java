package com.syntel.lms.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.syntel.lms.dao.MemberDaoImpl;
import com.syntel.lms.dto.Book;
import com.syntel.lms.dto.Transaction;
import com.syntel.lms.service.BookService;
import com.syntel.lms.service.BookServiceImpl;
import com.syntel.lms.service.MemberService;
import com.syntel.lms.service.MemberServiceImpl;
import com.syntel.lms.service.TransactionService;
import com.syntel.lms.service.TransactionServiceImpl;

public class TransactionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TransactionService transactionService = null;
	private MemberService memberService = null;
	private BookService bookService = null;

	public TransactionController() {
		System.out.println("Transaction controller created.....");
	}

	@Override
	public void init() throws ServletException {
		transactionService = new TransactionServiceImpl();
		memberService = new MemberServiceImpl();
		bookService = new BookServiceImpl();

		System.out.println("TransactionService Object created.....");
	}

	@Override
	public void destroy() {
		transactionService = null;
		memberService = null;
		bookService = null;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		RequestDispatcher rd = null;

		if (action.equals("CreateTransaction")) {

			request.setAttribute("memberList", memberService.getMemberList());

			List<Book> bookList = new ArrayList<>();
			bookList = bookService.getBookList();

			for (int i = 0; i < bookList.size(); i++) {
				if (transactionService.getTransactionByAccessionNo(bookList.get(i).getAccessionNo()))
					bookList.remove(i);
			}
			request.setAttribute("bookList", bookList);
			request.setAttribute("value", "createtransaction");
			request.getRequestDispatcher("librarianhome.jsp").forward(request, response);

		} else if (action.equals("ShowAllTransaction")) {
			request.setAttribute("transactionList", transactionService.getAllTransactionList());
			request.setAttribute("value", "showalltransaction");
			request.setAttribute("message", "All Transaction List");

			request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
		} else if (action.equals("CloseTransaction")) {
			request.setAttribute("transactionList", transactionService.getAllTransactionList());
			request.setAttribute("value", "closetransaction");
			request.setAttribute("message", "Select transaction");
			request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		RequestDispatcher rd = null;

		if (action.equals("IssueBook")) {
			System.out.println("In issue book");
			int memberId = Integer.parseInt(request.getParameter("memberId"));
			String accessionNo = request.getParameter("accessionNo");
			if (bookService.isBookAvailable(accessionNo)) {
				System.out.println("Yes book is available");
				if (bookService.getNoOfBorrowedBooksByMember(memberId) < 2
						&& memberService.getMember(memberId).getType().equals("Student")) {
					System.out.println("Yes student is eligible to borrwo book");
					// bookService.issueBook(accessionNo, memberId);
					bookService.updateBookStatus("Not Available", accessionNo);
					System.out.println("Book status updated");
					System.out.println("Memberid: " + memberId + " accessionNo " + accessionNo);
					String msg = transactionService.createTransaction(memberId, accessionNo);
					request.setAttribute("message", msg);

				} else if (bookService.getNoOfBorrowedBooksByMember(memberId) < 5
						&& memberService.getMember(memberId).getType().equals("Staff")) {
					System.out.println("Yes staff is eligible for book issue");
					bookService.updateBookStatus("Not Available", accessionNo);
					System.out.println("Book status updated");
					System.out.println("Memberid: " + memberId + " accessionNo " + accessionNo);
					String msg = transactionService.createTransaction(memberId, accessionNo);
					request.setAttribute("message", msg);
				} else {
					request.setAttribute("message", "Member : " + memberService.getMember(memberId).getName()
							+ " is not eligible for book issue");

				}
			} else {
				request.setAttribute("message", "Sorry...! Book is not available");
			}
			request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
		}

		else if (action.equals("ReturnBook")) {
			System.out.println("In return book.....");
			String[] transactionArray = request.getParameterValues("element");
			System.out.println(transactionArray.toString());
			for (int i = 0; i < transactionArray.length; i++) {

				if (transactionService.getTransactionById(transactionArray[i]).getStatus().equals("Open")) {
					System.out.println("Going for transactions......");
					Date returnDate = new Date();
					System.out.println(returnDate);
					Transaction t = transactionService.getTransactionById(transactionArray[i]);
					System.out.println(t.toString());
					Date dueDate = t.getDueDate();
					System.out.println(dueDate);
					long diff = returnDate.getTime() - dueDate.getTime();
					System.out.println(diff);
					diff = (diff) / (24 * 60 * 60 * 1000);
					System.out.println(diff);
					if (((memberService.getMember(t.getMemberId())).getType().equals("Student"))) {
						if (diff > 0)
							t.setFineAmount(diff);
						else
							t.setFineAmount(0);
						System.out.println("In student calculation...");
						java.sql.Date rdt = new java.sql.Date(returnDate.getTime());
						t.setReturnDate(rdt);
						System.out.println(t.getReturnDate());
						System.out.println(t.getFineAmount());
						System.out.println(bookService.updateBookStatus("Available", t.getAccessionNo()));
						System.out.println(transactionService.closeTransaction(t.getTransactionId(), t.getReturnDate(),
								t.getFineAmount()));
						;
						request.setAttribute("message", "Student " + memberService.getMember(t.getMemberId()).getName()
								+ " returned book successfully...\n Take Fine Rs. " + t.getFineAmount() + " /- Only");
						request.getRequestDispatcher("librarianhome.jsp").forward(request, response);

					} else if (((memberService.getMember(t.getMemberId())).getType().equals("Staff"))) {
						System.out.println("In staff calculation");
						java.sql.Date rdt = new java.sql.Date(returnDate.getTime());
						t.setReturnDate(rdt);
						System.out.println(t.getReturnDate());
						t.setFineAmount(0);
						System.out.println(t.getFineAmount());
						System.out.println(bookService.updateBookStatus("Available", t.getAccessionNo()));
						System.out.println(transactionService.closeTransaction(t.getTransactionId(), t.getReturnDate(),
								t.getFineAmount()));
						;
						request.setAttribute("message", "Staff " + memberService.getMember(t.getMemberId()).getName()
								+ " returned book successfully...");
						request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
					}
				} else {
					request.setAttribute("message", "Book Status is already close");
					request.getRequestDispatcher("librarianhome.jsp").forward(request, response);
				}
			}
		}
	}

}
