<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Logout</title>
<link href="LibrarianHome.css" rel="stylesheet" />
</head>
<body>
<div align="center">
	<div class="w3-container">
		<div class="center">
			<div class="w3-card-4">
				<div class="w3-container w3-green">
					<h2>Thank You !</h2>
				</div>
				<br/><br/>
				
				You have Logged out successfully...
				
				<br/><br/>
			</div>
		</div>
	</div>


	<c:if test="${value==null}">


		<%
			request.setAttribute("message",
						"This is not the right way to logout!!!\nHit on that 'GREEN POWER BUTTON'");
				if (request.getSession().getAttribute("memberType").equals("Librarian"))
					request.getRequestDispatcher("libriarianhome.jsp").forward(request, response);
				else
					request.getRequestDispatcher("memberhome.jsp").forward(request, response);
		%>

	</c:if>

	<c:if test="${value=='logout'}">

		<%
			request.getSession().invalidate();
		%>

	</c:if>
	<br />
	<br />
	<br />
	<a href="index.jsp">Click here to login again</a>
	
	</div>
</body>
</html>