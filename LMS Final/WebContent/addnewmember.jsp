<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Library Management System</title>
<style>
.w3-container:after,.w3-container:before
{
	content:"";
	display:table;
	clear:both;
	
}
	
.w3-container,.w3-panel
{
	padding:0.01em 16px;
}
	
.w3-card-4
{
	box-shadow:0 4px 10px 0 rgba(0,0,0,0.2),0 4px 20px 0 rgba(0,0,0,0.19);
}
	
.w3-green
{
	color:#fff!important;
	background-color:#4CAF50!important;
}
	
.w3-input{
	padding:8px;
	display:block;
	border:none;
	border-bottom:1px solid #ccc;
	width:95%;
}
.button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}
.center{
	margin:auto;
	width:60%;
	height:100%;
	padding:10px;
}

.select {
  width: 97%;
  padding: 5px 5px 5px 5px;
  font-size: 16px;
  border: 1px solid #ccc;
  height: 34px;
  appearance: none;
}
</style>

</head>
<body>

<div class="w3-container">
  <div class="center">
  <div class="w3-card-4">
    <div class="w3-container w3-green">
      <h2>Enter New Member Details</h2>
    </div>

    <form class="w3-container" action="MemberController?action=AddNewMember" method="post">
      <p>
      <input class="w3-input" placeholder="Enter Member Id" type="text" name="memberId" autofocus>
      </p>
      <p>     
      <input class="w3-input" placeholder="Enter Name" type="text" name="memberName">
      </p>
      <p> 
      <input class="w3-input" placeholder="Enter Address" type="text" name="memberAddress">
     </p>
      <p> 
      <font color="#666666">Select Member Type </font> <select name ="type" class="select">
			<option  value = "Staff">Staff</option>
			<option  value = "Student">Student</option>
			<option  value = "Librarian">Librarian</option>
			</select>
		</p>
		<p> 
      <input class="w3-input" placeholder="Enter Password" type="password" name="password">
      </p>
      
      <input class="button" type="submit" value="Add Member">
    </form>
  </div>
</div>
</div>


<body>
</html>