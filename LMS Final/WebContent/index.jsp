<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
<link href="LoginPage.css" rel="stylesheet" />
<style>


.background{
	content: "";
  background: url(bg.jpg);
  opacity: 0.4;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: -1;
}
</style>
</head>
<body>
	<div class="background"></div>
		<div class="center">
		<font color="red" size="6">${message}</font>
			<form action="MemberController?action=Login" method="post">

				<div class="imgcontainer">
					<img src="img_avatar2.png" alt="Syntel" class="avatar">
				</div>
				<div class="retro">Library Management System</div>
				<div class="container">

					<input type="text" placeholder="Enter MemberId" name="memberId"
						required autofocus> <input type="password"
						placeholder="Enter Password" name="password" required> <br />
					<br /> <select name="type" class="select">
						<option hidden>Select Member Type</option>
						<option value="Staff">Staff</option>
						<option value="Student">Student</option>
						<option value="Librarian">Librarian</option>
					</select> <br /> <br />
					

				</div>

				<div class="container" style="background-color: #f1f1f1">
					<button type="submit">Login</button>
				</div>
			</form>
		</div>
	
	<footer >
	<div align="center">&copy;Syntel Pvt. Ltd.</div>

	</footer>
</body>
</html>