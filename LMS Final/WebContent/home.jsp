<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Library Management System</title>
<link href="LibrarianHome.css" rel="stylesheet" />
</head>
<%
	if (!request.getSession().getAttribute("memberType").equals("Librarian")) {
		request.setAttribute("message1", "You are not allowed to access!!!");
		request.getRequestDispatcher("memberhome.jsp").forward(request, response);
	}
%>
<body>

	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div class="dropdown">
			<button class="dropbtn">Member</button>
			<div class="dropdown-content">
				<a href="MemberController?action=ViewAllMembers">View All</a> <a
					href="MemberController?action=ADDMEMBER">Add</a> <a
					href="MemberController?action=UPDATEMEMBER">Update</a> <a
					href="MemberController?action=DELETEMEMBER">Delete</a>

			</div>
		</div>

		<div class="dropdown">
			<button class="dropbtn">Book</button>
			<div class="dropdown-content">
				<a href="BookController?action=GetBookList">View All</a> <a
					href="BookController?action=ADDBOOK">Add</a> <a
					href="BookController?action=DELETEBOOK">Delete</a> <a
					href="BookController?action=GETBOOKBYAUTHOR">Author</a> <a
					href="BookController?action=GETBOOKBYTITLE">Title</a>
			</div>
		</div>

		<div class="dropdown">
			<button class="dropbtn">Transaction</button>
			<div class="dropdown-content">
				<a href="TransactionController?action=ShowAllTransaction">View
					All</a> <a href="TransactionController?action=CreateTransaction">Issue
					Book</a> <a href="TransactionController?action=CloseTransaction">Return
					Book</a>
			</div>
		</div>
	</div>




	<div class="topnav">
		<a href="javascript:void(0)" onclick="openNav()">&#9776;</a> <a
			href="librarianhome.jsp">Home</a> <a href="#contact">About Us</a> <a
			style="float: right" class="active"
			href="MemberController?action=LOGOUT"><img
			style="padding: none; height: 23px; width: 23px" src="Power.png"></a>


		<font style="padding: 12px; float: right" color="white" size="4">
			Hi ${librarianName }! </font>
	</div>
	<div align="center">
		<br /> <br /> <br /> Welcome to Librarian Home <br /> <br /> <font
			color="green">${message }</font>

		<c:if test="${value=='viewallmembers'}">
			<div align="center">
				<form action="MemberController?action=ViewAllMembers" method="post">
					<c:if test="${memberList!=null}">
						<table>
							<thead>
								<tr>
									<th>Member Id</th>
									<th>Name</th>
									<th>Address</th>
									<th>Member Type</th>

								</tr>
							</thead>
							<c:forEach var="member" items="${memberList}">
								<tr>
									<td>${member.memberId}</td>
									<td>${member.name}</td>
									<td>${member.address}</td>
									<td>${member.type}</td>


								</tr>
							</c:forEach>
						</table>
					</c:if>
				</form>
			</div>
		</c:if>

		<c:if test="${value=='addmnewember'}">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Enter New Member Details</h2>
						</div>

						<form class="w3-container"
							action="MemberController?action=AddNewMember" method="post">
							<p>
								<input class="w3-input" placeholder="Enter Member Id"
									type="text" name="memberId" autofocus required>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Name" type="text"
									name="memberName">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Address" type="text"
									name="memberAddress">
							</p>
							<p>

								<select name="type" class="select">
									<option disabled selected hidden>Select Member Type</option>
									<option value="Staff">Staff</option>
									<option value="Student">Student</option>
									<option value="Librarian">Librarian</option>
								</select>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Password"
									type="password" name="password">
							</p>
							<input class="button" type="submit" value="Add Member">
						</form>
					</div>
				</div>
			</div>
			${message}
		</c:if>

		<c:if test="${value=='updatemember' }">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Enter Member Details</h2>
						</div>

						<form class="w3-container"
							action="MemberController?action=UpdateMember" method="post">
							<p>
								Select Member ID:<br />
								<c:if test="${memberList!=null}">
									<select name="element" class="type" required>
										<c:forEach var="member" items="${memberList}">
											<option value="${member.memberId}">${member.memberId}</option>
										</c:forEach>
									</select>
								</c:if>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Name" type="text"
									name="memberName">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Address" type="text"
									name="memberAddress">
							</p>
							<p>

								<select name="type" class="select">
									<option hidden>Select Member Type</option>
									<option value="Staff">Staff</option>
									<option value="Student">Student</option>
									<option value="Librarian">Librarian</option>
								</select>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Password"
									type="password" name="password">
							</p>
							<input class="button" type="submit" value="Update Member">
						</form>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${value=='deletemember' }">
			<div align="center">
				<form action="MemberController?action=DeleteMember" method="post">
					<c:if test="${memberList!=null}">
						${message}
						<table>
							<thead>
								<tr>
									<th>Member Id</th>
									<th>Name</th>
									<th>Address</th>
									<th>Member Type</th>
									<th>Select</th>
								</tr>
							</thead>
							<c:forEach var="member" items="${memberList}">
								<tr>
									<td>${member.memberId}</td>
									<td>${member.name}</td>
									<td>${member.address}</td>
									<td>${member.type}</td>
									<td><input type="checkbox" name="element"
										value="${member.memberId}" /></td>

								</tr>
							</c:forEach>
						</table>
					</c:if>
					<br /> <br />

					<button type="submit" class="btn">Delete</button>
				</form>
			</div>
		</c:if>

		<c:if test="${value=='addbook' }">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Enter New Book Details</h2>
						</div>

						<form class="w3-container"
							action="BookController?action=AddNewBook" method="post">
							<p>
								<input class="w3-input" placeholder="Enter Accession no"
									type="text" name="accessionNo" autofocus required>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Title" type="text"
									name="title">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Author" type="text"
									name="author">
							</p>

							<input class="button" type="submit" value="Add Book">
						</form>
					</div>
				</div>
			</div>
		</c:if>


		<c:if test="${value=='deletebook' }">
			<div align="center">
				<form action="BookController?action=DeleteBook" method="post">
					<c:if test="${bookList!=null}">
						${message}
						<table>
							<thead>
								<tr>
									<th>Accession No</th>
									<th>Title</th>
									<th>Author</th>
									<th>Status</th>
									<th>Select</th>
								</tr>
							</thead>
							<c:forEach var="book" items="${bookList}">
								<tr>
									<td>${book.accessionNo}</td>
									<td>${book.title}</td>
									<td>${book.author}</td>
									<td>${book.status}</td>
									<td><input type="checkbox" name="element"
										value="${book.accessionNo}" /></td>

								</tr>
							</c:forEach>
						</table>
					</c:if>
					<br /> <br />

					<button type="submit" class="btn">Delete</button>
				</form>
			</div>
		</c:if>


		<c:if test="${value=='borrowedbooklist' }">

		</c:if>

		<c:if test="${value=='dbbt'}">
			<div align="center">
				<c:if test="${bookList!=null}">

					<table>
						<thead>
							<tr>
								<th>Accession No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Status</th>
							</tr>
						</thead>
						<c:forEach var="book" items="${bookList}">
							<tr>
								<td>${book.accessionNo}</td>
								<td>${book.title}</td>
								<td>${book.author}</td>
								<td>${book.status}</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</c:if>

		<c:if test="${value=='dbba'}">
			<div align="center">
				<c:if test="${bookList!=null}">

					<table>
						<thead>
							<tr>
								<th>Accession No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Status</th>
							</tr>
						</thead>
						<c:forEach var="book" items="${bookList}">
							<tr>
								<td>${book.accessionNo}</td>
								<td>${book.title}</td>
								<td>${book.author}</td>
								<td>${book.status}</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</c:if>


		<c:if test="${value=='getbookbyauthor'}">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Search Book by Author</h2>
						</div>
						<form class="w3-container"
							action="BookController?action=DisplayBookByAuthor" method="post">
							<br /> <br /> <input class="w3-input" type="text"
								name="authorName" list="datalist1"
								placeholder="Enter Author Na me" />
							<datalist id="datalist1"> <c:forEach var="book"
								items="${bookList}">
								<option value="${book.author}"></option>
							</c:forEach> </datalist>
							<br /> <input type="submit" class="button"
								value="Get Book Details">
						</form>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${value=='getbookbytitle' }">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Search Book by Title</h2>
						</div>
						<form class="w3-container"
							action="BookController?action=DisplayBookByTitle" method="post">
							<br /> <br /> <input class="w3-input" type="text" name="title"
								list="datalist1" placeholder="Enter Book Title" />
							<datalist id="datalist1"> <c:forEach var="book"
								items="${bookList}">
								<option value="${book.title}"></option>
							</c:forEach> </datalist>
							<br /> <input type="submit" class="button"
								value="Get Book Details">
						</form>
					</div>
				</div>
			</div>
		</c:if>



		<c:if test="${value=='viewallbooks' }">
			<div align="center">
				<c:if test="${bookList!=null}">

					<table>
						<thead>
							<tr>
								<th>Accession No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Status</th>
							</tr>
						</thead>
						<c:forEach var="book" items="${bookList}">
							<tr>
								<td>${book.accessionNo}</td>
								<td>${book.title}</td>
								<td>${book.author}</td>
								<td>${book.status}</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</c:if>

		<c:if test="${value=='createtransaction'}">
			<div align="center">
				<br />${message}<br />
				<div class="w3-container">
					<div class="center">
						<div class="w3-card-4">
							<div class="w3-container w3-green">
								<h2>Issue Book</h2>
							</div>
							<form class="w3-container"
								action="TransactionController?action=IssueBook" method="post">
								<p>
									<select class="select" name="memberId">
										<option hidden>Select MemberId</option>
										<c:forEach var="m" items="${memberList}">
											<option value="${m.memberId}">${m.memberId}</option>
										</c:forEach>
									</select>
								</p>
								<p>
									<br /> <select class="select" name="accessionNo">
										<option hidden>Select Accession No</option>
										<c:forEach var="b" items="${bookList}">
											<option value="${b.accessionNo }">${b.accessionNo}</option>
										</c:forEach>
									</select>
								</p>
								<input class="button" type="submit" value="Issue Book">

							</form>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${value=='showalltransaction'}">
			<div align="center">
				<br />${message}<br />
				<table>
					<thead>
						<tr>
							<th>Transaction Id</th>
							<th>Member Id</th>
							<th>Accession No</th>
							<th>Issue Date</th>
							<th>Due Date</th>
							<th>Return Date</th>
							<th>Fine Amount</th>
							<th>Book Status</th>
						</tr>
					</thead>
					<c:forEach var="transaction" items="${transactionList}">
						<tr>
							<td>${transaction.transactionId}</td>
							<td>${transaction.memberId}</td>
							<td>${transaction.accessionNo}</td>
							<td>${transaction.issueDate}</td>
							<td>${transaction.dueDate}</td>
							<td>${transaction.returnDate}</td>
							<td>${transaction.fineAmount}</td>
							<td>${transaction.status}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>

		<c:if test="${value=='closetransaction'}">
			<form action="TransactionController?action=ReturnBook" method="post">
				<div align="center">
					<br />${message}<br />
					<table>
						<thead>
							<tr>
								<th>Transaction Id</th>
								<th>Member Id</th>
								<th>Accession No</th>
								<th>Issue Date</th>
								<th>Due Date</th>
								<th>Return Date</th>
								<th>Fine Amount</th>
								<th>Book Status</th>
								<th>Select</th>
							</tr>
						</thead>
						<c:forEach var="transaction" items="${transactionList}">
							<tr>
								<td>${transaction.transactionId}</td>
								<td>${transaction.memberId}</td>
								<td>${transaction.accessionNo}</td>
								<td>${transaction.issueDate}</td>
								<td>${transaction.dueDate}</td>
								<td>${transaction.returnDate}</td>
								<td>${transaction.fineAmount}</td>
								<td>${transaction.status}</td>
								<td><input type="checkbox" name="element"
									value="${transaction.transactionId }"></td>
							</tr>
						</c:forEach>
					</table>
					<br /> <br />
					<button type="submit" class="btn">Return Book</button>

				</div>
			</form>
		</c:if>

		<script>
			function openNav() {
				document.getElementById("mySidenav").style.width = "250px";
				document.getElementById("main").style.marginLeft = "250px";
				document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
			}

			function closeNav() {
				document.getElementById("mySidenav").style.width = "0";
				document.getElementById("main").style.marginLeft = "0";
				document.body.style.backgroundColor = "white";
			}
		</script>



	</div>
	</div>

	<div class="footer">
		<img style="align: left; width: 180px; height: 45px"
			src="img_avatar2.png" alt="Syntel">
	</div>

</body>
</html>