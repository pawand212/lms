<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Library Management System</title>

<link href="LibrarianHome.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css"
	href="./css/1505382173index(2).css" media="all">

<link href="LibrarianHome.css" rel="stylesheet" />
<style>
body, html {
	height: 100%;
	margin: 0;
}

.bgimg {
	background-image: url('bg.jpg');
	height: 25%;
	background-position: center;
	background-size: cover;
	position: relative;
	color: white;
	font-family: "Courier New", Courier, monospace;
	font-size: 25px;
}

.topleft {
	position: absolute;
}

.middle {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	text-align: center;
}

hr {
	margin: auto;
	width: 40%;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {

		$(".dropbtn").click(function() {
			var X = $(this).attr('id');
			if (X == 1) {
				$(".submenu").hide();
				$(this).attr('id', '0');
			} else {
				$(".submenu").show();
				$(this).attr('id', '1');
			}

		});

		//Mouse click on sub menu
		$(".submenu").mouseup(function() {
			return false
		});

		//Mouse click on my account link
		$(".dropbtn").mouseup(function() {
			return false
		});

		//Document Click
		$(document).mouseup(function() {
			$(".submenu").hide();
			$(".dropbtn").attr('id', '');
		});
	});
</script>
</head>
<%
	if (!request.getSession().getAttribute("memberType")
			.equals("Librarian")) {
		request.setAttribute("message1",
				"You are not allowed to access!!!");
		request.getRequestDispatcher("memberhome.jsp").forward(request,
				response);
	} else if (request.getSession().getAttribute("memberType")
			.equals(null)) {
		request.setAttribute("message1",
				"You are not allowed to access!!!");
		request.getRequestDispatcher("logout.jsp").forward(request,
				response);
	}
%>
<body>
	<div class="bgimg">
		<div class="topleft">
			<p>
				<img style="align: left; width: 180px; height: 45px"
					src="img_avatar2.png" alt="Syntel">
			</p>
		</div>
		<div class="middle">
			<h1>Library Management System</h1>
			<hr>
			<p id="demo" style="font-size: 30px"></p>
		</div>

	</div>

	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div class="dropdown">
			<button class="dropbtn">Member</button>
			<div class="dropdown-content">
				<a href="MemberController?action=ViewAllMembers">View All</a> <a
					href="MemberController?action=ADDMEMBER">Add</a> <a
					href="MemberController?action=UPDATEMEMBER">Update</a> <a
					href="MemberController?action=DELETEMEMBER">Delete</a>

			</div>
		</div>

		<div class="dropdown">
			<button class="dropbtn">Book</button>
			<div class="dropdown-content">
				<a href="BookController?action=GetBookList">View All</a> <a
					href="BookController?action=ADDBOOK">Add</a> <a
					href="BookController?action=DELETEBOOK">Delete</a> <a
					href="BookController?action=GETBOOKBYAUTHOR">Author</a> <a
					href="BookController?action=GETBOOKBYTITLE">Title</a>
			</div>
		</div>

		<div class="dropdown">
			<button class="dropbtn">Transaction</button>
			<div class="dropdown-content">
				<a href="TransactionController?action=ShowAllTransaction">View
					All</a> <a href="TransactionController?action=CreateTransaction">Issue
					Book</a> <a href="TransactionController?action=CloseTransaction">Return
					Book</a>
			</div>
		</div>
	</div>

	<div class="dropdown1" style="float: right;">
		<button class="dropbtn1">
			<img style="padding: none; height: 25px; width: 25px" src="Power.png">
		</button>
		<div class="dropdown-content1">

			<a href="MemberController?action=CHANGEPASSWORD">Change Password</a>
			<a href="MemberController?action=LOGOUT">Logout</a>

		</div>
	</div>

	<div class="topnav">
		<a href="javascript:void(0)" onclick="openNav()">&#9776;</a> <a
			href="MemberController?action=HOME">Home</a> <a href="#contact">About
			Us</a> <font style="padding: 12px; float: right" color="white" size="4">
			Hi ${librarianName }! </font>
	</div>

	<div align="center">
		<br /> <font color="green" size="5">${message }</font>

		<c:if test="${flag=='show'}">
			<!-- Content section -->
			<div class="section section-padding page-detail">
				<div class="container">
					<div class="row">
						<div id="page-content" class="col-md-12">
							<div id="post-2228"
								class="post-2228 page type-page status-publish hentry">
								<div class="section-page-content clearfix ">
									<div class="entry-content">
										<div
											class="vc_row wpb_row vc_row-fluid vc_custom_1467975728188">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div
															class="group-title-index shw-shortcode group-title-index-74247743959ba4fad973c8 ">
															<div class="choose-course ">
																<div class="row choose-course-wrapper ">
																	<div class="inline_block col-md-4 col-xs-6">
																		<div class="item-course item-22">
																			<div class="icon-circle">
																				<div class="icon-background">
																					<i class=""></i>
																				</div>
																				<div class="info">
																					<div class="info-back">
																						<a href="#" class="">${noOfStudent }</a>
																					</div>
																				</div>
																			</div>
																			<div class="name-course">
																				<a href="#">Students</a>

																			</div>
																		</div>
																	</div>
																	<div class="inline_block col-md-4 col-xs-6">
																		<div class="item-course item-24">
																			<div class="icon-circle">
																				<div class="icon-background">
																					<i class=""></i>
																				</div>
																				<div class="info">
																					<div class="info-back">
																						<a href="" class="">${noOfStaff }</a>
																					</div>
																				</div>
																			</div>
																			<div class="name-course">
																				<a href="#">Staff</a>

																			</div>
																		</div>
																	</div>
																	<div class="inline_block col-md-4 col-xs-6">
																		<div class="item-course item-32">
																			<div class="icon-circle">
																				<div class="icon-background">
																					<i class=""></i>
																				</div>
																				<div class="info">
																					<div class="info-back">
																						<a href="#" class="">${totalMembers }</a>
																					</div>
																				</div>
																			</div>
																			<div class="name-course">
																				<a href="#">Total Members</a>

																			</div>
																		</div>
																	</div>
																	<div class="inline_block col-md-4 col-xs-6">
																		<div class="item-course item-35">
																			<div class="icon-circle">
																				<div class="icon-background">
																					<i class=""></i>
																				</div>
																				<div class="info">
																					<div class="info-back">
																						<a href="#" class="">${booksAvailable }</a>
																					</div>
																				</div>
																			</div>
																			<div class="name-course">
																				<a href="#">Books Available</a>

																			</div>
																		</div>
																	</div>
																	<div class="inline_block col-md-4 col-xs-6">
																		<div class="item-course item-39">
																			<div class="icon-circle">
																				<div class="icon-background">
																					<i class=""></i>
																				</div>
																				<div class="info">
																					<div class="info-back">
																						<a href="#" class="">${booksNotAvailable }</a>
																					</div>
																				</div>
																			</div>
																			<div class="name-course">
																				<a href="#">Books Issued</a>

																			</div>
																		</div>
																	</div>
																	<div class="inline_block col-md-4 col-xs-6">
																		<div class="item-course item-40">
																			<div class="icon-circle">
																				<div class="icon-background">
																					<i class=""></i>
																				</div>
																				<div class="info">
																					<div class="info-back">
																						<a href="#" class="">${totalBooks }</a>
																					</div>
																				</div>
																			</div>
																			<div class="name-course">
																				<a href="#">Total Books</a>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div data-vc-full-width="true" data-vc-full-width-init="true"
											class="vc_row wpb_row vc_row-fluid sw-bg_parallax vc_custom_1452133480907 vc_row-has-fill"
											style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</c:if>

		<c:if test="${value=='viewallmembers'}">
			<div align="center">
				<form action="MemberController?action=ViewAllMembers" method="post">
					<c:if test="${memberList!=null}">
						<table>
							<thead>
								<tr>
									<th>Member Id</th>
									<th>Name</th>
									<th>Address</th>
									<th>Member Type</th>

								</tr>
							</thead>
							<c:forEach var="member" items="${memberList}">
								<tr>
									<td>${member.memberId}</td>
									<td>${member.name}</td>
									<td>${member.address}</td>
									<td>${member.type}</td>


								</tr>
							</c:forEach>
						</table>
					</c:if>
				</form>
			</div>
		</c:if>

		<c:if test="${value=='addmnewember'}">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Enter New Member Details</h2>
						</div>

						<form class="w3-container"
							action="MemberController?action=AddNewMember" method="post">
							<p>
								<input class="w3-input" placeholder="Enter Member Id"
									type="text" name="memberId" autofocus required>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Name" type="text"
									name="memberName">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Address" type="text"
									name="memberAddress">
							</p>
							<p>

								<select name="type" class="select">
									<option disabled selected hidden>Select Member Type</option>
									<option value="Staff">Staff</option>
									<option value="Student">Student</option>
									<option value="Librarian">Librarian</option>
								</select>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Password"
									type="password" name="password">
							</p>
							<input class="button" type="submit" value="Add Member">
						</form>
					</div>
				</div>
			</div>
			${message}
		</c:if>


		<c:if test="${value=='changepassword'}">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Password Change Page</h2>
						</div>

						<form class="w3-container"
							action="MemberController?action=ChangePassword" method="post">
							<br />
							<p>
								<input class="w3-input" placeholder="Enter Old Password"
									type="text" name="cPassword" autofocus required>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter New Password"
									type="text" name="nPassword1">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter New Password Again"
									type="text" name="nPassword2"> <br /> <input
									class="button" type="submit" value="Change Password">
						</form>
					</div>
				</div>
			</div>
			${message}
		</c:if>


		<c:if test="${value=='updatemember' }">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Enter Member Details</h2>
						</div>

						<form class="w3-container"
							action="MemberController?action=UpdateMember" method="post">
							<p>

								<c:if test="${memberList!=null}">
									<select class="select" name="element" class="type" required>
										<option hidden>Select Member Id</option>
										<c:forEach var="member" items="${memberList}">
											<option value="${member.memberId}">${member.memberId}</option>
										</c:forEach>
									</select>
								</c:if>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Name" type="text"
									name="memberName">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Address" type="text"
									name="memberAddress">
							</p>
							<p>

								<select name="type" class="select">
									<option hidden>Select Member Type</option>
									<option value="Staff">Staff</option>
									<option value="Student">Student</option>
									<option value="Librarian">Librarian</option>
								</select>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Password"
									type="password" name="password">
							</p>
							<input class="button" type="submit" value="Update Member">
						</form>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${value=='deletemember' }">
			<div align="center">
				<form action="MemberController?action=DeleteMember" method="post">
					<c:if test="${memberList!=null}">
						<table>
							<thead>
								<tr>
									<th>Member Id</th>
									<th>Name</th>
									<th>Address</th>
									<th>Member Type</th>
									<th>Select</th>
								</tr>
							</thead>
							<c:forEach var="member" items="${memberList}">
								<tr>
									<td>${member.memberId}</td>
									<td>${member.name}</td>
									<td>${member.address}</td>
									<td>${member.type}</td>
									<td><input type="checkbox" name="element"
										value="${member.memberId}" /></td>

								</tr>
							</c:forEach>
						</table>
					</c:if>
					<br /> <br />

					<button type="submit" class="btn">Delete</button>
				</form>
			</div>
		</c:if>

		<c:if test="${value=='addbook' }">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Enter New Book Details</h2>
						</div>

						<form class="w3-container"
							action="BookController?action=AddNewBook" method="post">
							<p>
								<input class="w3-input" placeholder="Enter Accession no"
									type="text" name="accessionNo" autofocus required>
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Title" type="text"
									name="title">
							</p>
							<p>
								<input class="w3-input" placeholder="Enter Author" type="text"
									name="author">
							</p>

							<input class="button" type="submit" value="Add Book">
						</form>
					</div>
				</div>
			</div>
		</c:if>


		<c:if test="${value=='deletebook' }">
			<div align="center">
				<form action="BookController?action=DeleteBook" method="post">
					<c:if test="${bookList!=null}">
						<table>
							<thead>
								<tr>
									<th>Accession No</th>
									<th>Title</th>
									<th>Author</th>
									<th>Status</th>
									<th>Select</th>
								</tr>
							</thead>
							<c:forEach var="book" items="${bookList}">
								<tr>
									<td>${book.accessionNo}</td>
									<td>${book.title}</td>
									<td>${book.author}</td>
									<td>${book.status}</td>
									<td><input type="checkbox" name="element"
										value="${book.accessionNo}" /></td>

								</tr>
							</c:forEach>
						</table>
					</c:if>
					<br /> <br />

					<button type="submit" class="btn">Delete</button>
				</form>
			</div>
		</c:if>


		<c:if test="${value=='borrowedbooklist' }">

		</c:if>

		<c:if test="${value=='dbbt'}">
			<div align="center">
				<c:if test="${bookList!=null}">

					<table>
						<thead>
							<tr>
								<th>Accession No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Status</th>
							</tr>
						</thead>
						<c:forEach var="book" items="${bookList}">
							<tr>
								<td>${book.accessionNo}</td>
								<td>${book.title}</td>
								<td>${book.author}</td>
								<td>${book.status}</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</c:if>

		<c:if test="${value=='dbba'}">
			<div align="center">
				<c:if test="${bookList!=null}">

					<table>
						<thead>
							<tr>
								<th>Accession No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Status</th>
							</tr>
						</thead>
						<c:forEach var="book" items="${bookList}">
							<tr>
								<td>${book.accessionNo}</td>
								<td>${book.title}</td>
								<td>${book.author}</td>
								<td>${book.status}</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</c:if>


		<c:if test="${value=='getbookbyauthor'}">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Search Book by Author</h2>
						</div>
						<form class="w3-container"
							action="BookController?action=DisplayBookByAuthor" method="post">
							<br /> <br /> <input class="w3-input" type="text"
								name="authorName" list="datalist1"
								placeholder="Enter Author Name" />
							<datalist id="datalist1"> <c:forEach var="book"
								items="${bookList}">
								<option value="${book.author}"></option>
							</c:forEach> </datalist>
							<br /> <input type="submit" class="button"
								value="Get Book Details">
						</form>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${value=='getbookbytitle' }">
			<div class="w3-container">
				<div class="center">
					<div class="w3-card-4">
						<div class="w3-container w3-green">
							<h2>Search Book by Title</h2>
						</div>
						<form class="w3-container"
							action="BookController?action=DisplayBookByTitle" method="post">
							<br /> <br /> <input class="w3-input" type="text" name="title"
								list="datalist1" placeholder="Enter Book Title" />
							<datalist id="datalist1"> <c:forEach var="book"
								items="${bookList}">
								<option value="${book.title}"></option>
							</c:forEach> </datalist>
							<br /> <input type="submit" class="button"
								value="Get Book Details">
						</form>
					</div>
				</div>
			</div>
		</c:if>



		<c:if test="${value=='viewallbooks' }">
			<div align="center">
				<c:if test="${bookList!=null}">

					<table>
						<thead>
							<tr>
								<th>Accession No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Status</th>
							</tr>
						</thead>
						<c:forEach var="book" items="${bookList}">
							<tr>
								<td>${book.accessionNo}</td>
								<td>${book.title}</td>
								<td>${book.author}</td>
								<td>${book.status}</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</c:if>

		<c:if test="${value=='createtransaction'}">
			<div align="center">

				<div class="w3-container">
					<div class="center">
						<div class="w3-card-4">
							<div class="w3-container w3-green">
								<h2>Issue Book</h2>
							</div>
							<form class="w3-container"
								action="TransactionController?action=IssueBook" method="post">
								<br />
								<p>
									<select class="select" name="memberId">
										<option hidden>Select MemberId</option>
										<c:forEach var="m" items="${memberList}">
											<option value="${m.memberId}">${m.memberId}</option>
										</c:forEach>
									</select>
								</p>
								<p>
									<br /> <select class="select" name="accessionNo">
										<option hidden>Select Accession No</option>
										<c:forEach var="b" items="${bookList}">
											<option value="${b.accessionNo }">${b.accessionNo}</option>
										</c:forEach>
									</select>
								</p>
								<input class="button" type="submit" value="Issue Book">

							</form>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${value=='showalltransaction'}">
			<div align="center">

				<table>
					<thead>
						<tr>
							<th>Transaction Id</th>
							<th>Member Id</th>
							<th>Accession No</th>
							<th>Issue Date</th>
							<th>Due Date</th>
							<th>Return Date</th>
							<th>Fine Amount</th>
							<th>Book Status</th>
						</tr>
					</thead>
					<c:forEach var="transaction" items="${transactionList}">
						<tr>
							<td>${transaction.transactionId}</td>
							<td>${transaction.memberId}</td>
							<td>${transaction.accessionNo}</td>
							<td>${transaction.issueDate}</td>
							<td>${transaction.dueDate}</td>
							<td>${transaction.returnDate}</td>
							<td>${transaction.fineAmount}</td>
							<td>${transaction.status}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>

		<c:if test="${value=='closetransaction'}">
			<form action="TransactionController?action=ReturnBook" method="post">
				<div align="center">
					<br />
					<br />
					<table>
						<thead>
							<tr>
								<th>Transaction Id</th>
								<th>Member Id</th>
								<th>Accession No</th>
								<th>Issue Date</th>
								<th>Due Date</th>
								<th>Return Date</th>
								<th>Fine Amount</th>
								<th>Book Status</th>
								<th>Select</th>
							</tr>
						</thead>
						<c:forEach var="transaction" items="${transactionList}">
							<tr>
								<td>${transaction.transactionId}</td>
								<td>${transaction.memberId}</td>
								<td>${transaction.accessionNo}</td>
								<td>${transaction.issueDate}</td>
								<td>${transaction.dueDate}</td>
								<td>${transaction.returnDate}</td>
								<td>${transaction.fineAmount}</td>
								<td>${transaction.status}</td>
								<td><input type="radio" name="element"
									value="${transaction.transactionId }"></td>
							</tr>
						</c:forEach>
					</table>
					<br /> <br />
					<button type="submit" class="btn">Return Book</button>

				</div>
			</form>
		</c:if>

		<script>
			function openNav() {
				document.getElementById("mySidenav").style.width = "250px";
				document.getElementById("main").style.marginLeft = "250px";
				document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
			}

			function closeNav() {
				document.getElementById("mySidenav").style.width = "0";
				document.getElementById("main").style.marginLeft = "0";
				document.body.style.backgroundColor = "white";
			}
		</script>



	</div>
	</div>
	<div class="home">
		<%
			if (request.getParameter("action").equals("home")) {
		%>
		<jsp:include page="mainLib.jsp"></jsp:include>
		<%
			}
		%>
	</div>
	<br />
	<div style="height: 35px" class="footer">
		<font size=3.5px>&copy;Syntel Pvt. Ltd.</font>
	</div>

</body>
</html>