<!DOCTYPE html>
<html >
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Syntel's LMS</title>
<link href="LibrarianHome.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href=".css/1502849973index(3).css" media="all">

<link rel="stylesheet" type="text/css" href="./css/1502849973index.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/1502849973index(1).css" media="screen">
<link rel="stylesheet" type="text/css" href="./css/1502849973index(2).css" media="all">

<link rel="stylesheet" type="text/css" href="./css/1505382173index.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/1505382173index(1).css" media="only screen and (max-width: 768px)">
<link rel="stylesheet" type="text/css" href="./css/1505382173index(2).css" media="all">

<!-- Start Dynamic Styling -->
<body >
<div align="center">
<h4 class="top-title ">Welcome ${librarianName} <br/><br/>to</h4><h2 class="center-title ">Syntel's Library Management System</h2>
</div>
<!-- Content section -->
<div class="section section-padding page-detail">
<div class="container">
<div class="row">
<div id="page-content" class="col-md-12">
<div id="post-2228" class="post-2228 page type-page status-publish hentry">
<div class="section-page-content clearfix ">
 <div class="entry-content"> 
<div class="vc_row wpb_row vc_row-fluid vc_custom_1467975728188"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
<div class="group-title-index shw-shortcode group-title-index-74247743959ba4fad973c8 ">
<div class="choose-course ">
<div class="row choose-course-wrapper ">
<div class="inline_block col-md-4 col-xs-6">
<div class="item-course item-35">
<div class="icon-circle">
<div class="icon-background"><i class=""></i></div>
<div class="info">
<div class="info-back">
<a href="#" class="">8</a>
</div>
</div>
</div>
<div class="name-course">
<a href="#">Books Available</a>

</div>
</div>
</div>
<div class="inline_block col-md-4 col-xs-6">
<div class="item-course item-39">
<div class="icon-circle">
<div class="icon-background"><i class=""></i></div>
<div class="info">
<div class="info-back">
<a href="#" class="">5</a>
</div>
</div>
</div>
<div class="name-course">
<a href="#">Books Issued</a>

</div>
</div>
</div>
<div class="inline_block col-md-4 col-xs-6">
<div class="item-course item-40">
<div class="icon-circle">
<div class="icon-background"><i class=""></i></div>
<div class="info">
<div class="info-back">
<a href="#" class="">9</a>
</div>
</div>
</div>
<div class="name-course">
<a href="#">Total Books</a>

</div>
</div>
</div>
</div>
</div>
</div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid sw-bg_parallax vc_custom_1452133480907 vc_row-has-fill" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
</div></div></div>
</div></div></div>
</div></div></div>
</div>
</div>


</body></html>